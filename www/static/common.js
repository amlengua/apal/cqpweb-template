/* Constants */

const debug = true; // Sets debug mode

/* Common functions */

// Prints to console if debug == true
function puts (...args) {
  if (debug) { console.log("[DEBUG]", ...args) }
}

puts("Test");

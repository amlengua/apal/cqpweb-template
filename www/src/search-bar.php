<?php
  # Adds CQPweb required libraries
  require CQPWEB_ROOT."lib/environment.php";
  require CQPWEB_ROOT."lib/sql-lib.php";
  require CQPWEB_ROOT."lib/general-lib.php";
  require CQPWEB_ROOT."lib/useracct-lib.php";
  require CQPWEB_ROOT."lib/corpus-lib.php";
  require CQPWEB_ROOT."lib/exiterror-lib.php";

  # Retrieves CQPweb data
  $User = $Config = NULL;
  cqpweb_startup_environment(CQPWEB_STARTUP_DONT_CONNECT_CQP, RUN_LOCATION_MAINHOME);
  $corpora_info = get_all_corpora_info();
  cqpweb_shutdown_environment();
?>
<form class="search-bar" action="src/search.php" method="get">
  <?php array_key_exists("error", $_GET) ? box($_GET["error"], "error") : NULL ?>
  <?php array_key_exists("warn", $_GET) ? box($_GET["warn"], "warn") : NULL ?>
  <div class="field has-addons has-addons-centered">
    <p class="control">
      <span class="select">
        <select name="corpus">
          <?php
            foreach($corpora_info as $key => $val) {
              $corpus = (array)$val;
              $id = $corpus["corpus"];
              $name = $corpus["title"];
              $selected = array_key_first($corpora_info) == $key ? "selected" : "";
              echo "<option value='$id' $selected>$name</option>";
            }
          ?>
        </select>
      </span>
    </p>
    <p class="control">
      <span class="select">
        <select name="mode">
          <option value="sq_nocase" selected><?php p("search_nocase")?></option>
          <option value="sq_case"><?php p("search_case")?></option>
          <option value="cqp"><?php p("search_cqp")?></option>
        </select>
      </span>
    </p>
    <p class="control is-expanded">
      <input name="query" class="input" type="text" placeholder="<?php p("search_placeholder")?>" autofocus>
    </p>
    <p class="control submit">
      <button class="button is-primary" type="submit" value=""/>
        <svg width="20" height="20" fill="white" class="bi bi-search" viewBox="0 0 16 16">
          <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
        </svg>
      </button>
    </p>
  </div>
</form>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>
    <?php p($page."_title")?> |
    <?php p("general_title")?> |
    <?php p("general_owner_name")?>
  </title>
  <link rel="shortcut icon" href="static/favicon.png">
  <link rel="apple-touch-icon" href="static/favicon.png">
  <!-- CSS uses Bulma, cfr. https://bulma.io -->
  <link rel="stylesheet" href="static/bulma.min.css" />
  <script src="static/common.js"></script>
</head>

<!--
Mexican Academy of Language Template
(c) 2023 perro tuerto <hi@perrotuerto.blog>
Founded by Mexican Academy of Language <https://academia.org.mx>
Code under GPLv3 <https://gitlab.com/amlengua/apal/cqpweb-template>
-->

<?php
  # Includes enviroment
  include(__DIR__."/enviroment.php");

  # Builds HTML
  include(TEMPLATE_ROOT."src/head.php");
  echo "<!DOCTYPE html>";
  echo "<html>";
  echo "<body id='$page'>";
  include(TEMPLATE_ROOT."src/nav.php");
  include(TEMPLATE_ROOT."src/$page.php");
  include(TEMPLATE_ROOT."src/quick-links.php");
  include(TEMPLATE_ROOT."src/footer.php");
  echo "</body>";
  echo "</html>";
?>

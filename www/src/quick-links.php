<div class="quick-links">
  <div>
    <span>👥</span>
    <span>
      <a href=""><?php p("quicklinks_credits")?></a>
    </span>
  </div>
  <div>
    <span>👤</span>
    <span>
      <a href=""><?php p("quicklinks_login")?></a>
    </span>
  </div>
  <div>
    <span>❓</span>
    <span>
      <a href=""><?php p("quicklinks_help")?></a>
    </span>
  </div>
</div>

<?php
  # PHP for the whole template

  # ONLY for debbuging: Prints errors
  error_reporting(E_ALL ^ E_WARNING);
  ini_set("display_errors", "1");

  # Constants
  define("TEMPLATE_ROOT", realpath(__DIR__."/..")."/");
  define("CQPWEB_ROOT", realpath(TEMPLATE_ROOT."/..")."/");

  # Add dependencies
	require_once TEMPLATE_ROOT."/lib/spyc.php";
	require_once TEMPLATE_ROOT."/lib/i18n.php";

  # Enables internacionalization and localization
  # Cfr. https://github.com/Philipp15b/php-i18n
  $i18n = new i18n();
  $i18n->setFilePath(TEMPLATE_ROOT."locale/{LANGUAGE}.yaml");
  $i18n->setCachePath(TEMPLATE_ROOT."cache");
  $i18n->setFallbackLang('en');
	$i18n->init();

  # Common functions

  # Adds notification box
  function box($key, $type) {
    $msg = L($type."_".$key);
    $classes = array("error"=>"danger", "warn"=>"warning", "ok"=>"success");
    $class = array_key_exists($type, $classes) ? $classes[$type] : "info";
    $html = "<div class='box notification is-$class has-text-centered'>";
    $html .= "<b>$msg</b>";
    $html .= "</div>";
    echo $html;
  }

  # Parses canonical CQPweb URL
  function parse_cqpweb_url() {
    $parts = parse_url(L("general_base_cqpweb"));
    $port = "";
    if (array_key_exists("port", $parts)) {
      $port = ":".$parts["port"];
    }
    return $parts["scheme"]."://".$parts["host"].$port."/";
  }

  # Constants that requires previous code
  define("CQPWEB_URL", parse_cqpweb_url());
?>

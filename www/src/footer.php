<footer class="footer content has-text-centered is-small">
  <p>
    <?php p("footer_year")?>
    <a href="<?php p("general_owner_url")?>" target="_blank">
      <?php p("general_owner_name")?>
    </a>
  <p>
  </p>
    <?php p("footer_template_pre")?>:
    <a href="<?php p("footer_template_url")?>" target="_blank">
      <?php p("footer_template_name")?>
    </a>
  <p>
  </p>
    <?php p("footer_corpus_pre")?>:
    <a href="<?php p("footer_corpus_url")?>" target="_blank">
      <?php p("footer_corpus_name")?>
    </a>
  </p>
</footer>

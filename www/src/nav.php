<nav id="nav" class="navbar hero is-primary is-small" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="<?php p("general_base_template")?>">
      <img src="<?php p("general_favicon")?>" width="28" height="28">
    </a>
    <div class="hero-body">
      <p class="title is-size-4"><?php p("general_title")?></p>
      <p class="subtitle is-size-6"><?php p("general_subtitle")?></p>
    </div>
  </div>
</nav>

<?php
  include(__DIR__."/enviroment.php");

  $location = "Location: ./../";

  if (empty($_GET["query"])) {
    $location .= "?error=empty_query";
    # TODO
    header($location);
  } else {
    /* Prueba 1: Uso CQPweb */
    $corpus = $_GET["corpus"];
    $query = http_build_query(array("theData" => $_GET["query"], "qmode" => $_GET["mode"], "pp" => 100));

    $token = array("f" => "log_in", "persist" => true, "username" => "anonimo", "password" => "anonimo123");
    $token = http_build_query($token);
    $token = CQPWEB_URL."$corpus/api.php?$token";
    $token = json_decode(file_get_contents($token));
    $token = $token->{"user_login_token"};

    ob_start();
    $url = CQPWEB_URL."$corpus/concordance.php?$query";
    $opts = array(
      "http" => array(
        "method" => "GET",
        "header" => "Cookie: CQPwebLogonToken=$token\r\n"
      )
    );
    $context = stream_context_create($opts);
    echo file_get_contents($url, false, $context);
    $output = ob_get_clean();

    /*
    $dom = new DOMDocument();
    $dom->loadHTML($output);
    $documentElement = $dom->documentElement;

    echo $dom->saveHTML();
     */

    echo $output;

    /* Prueba 2: uso directo de cqp */
    $corpus = strtoupper($_GET["corpus"]);
    $query = $_GET["query"];
    $query = $_GET["mode"] == "sq_nocase" ? "\"$query\" %cd;" : "\"$query\";";
    $query = "set ShowTagAttributes on; $query";
    exec("cqpcl -r /cqpweb_files/registry -D $corpus '$query'", $stdout);
    foreach ($stdout as &$line) {
      echo htmlentities($line) . "<br>";
    }

    #$result = http_build_query(array("html" => $output));

    #$location .= "result.php?$result";
  }

  #header($location);
?>

# Builds CSS

# Goes to root directory
cd $(dirname $0)/..

# Compiles Sass
sassc -t compressed conf/aml.scss www/static/bulma.min.css

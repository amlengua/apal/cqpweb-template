# Add template to CQPweb

# Variables
IMG="cqpweb"
PUBLIC="/var/www/html"
TEMPLATE="$PUBLIC/aml"

# Goes to root directory
cd $(dirname $0)/..

# Deletes old template
docker exec -it $IMG rm -rf $TEMPLATE

# Add template
docker cp www $IMG:$TEMPLATE

# Changes ownership
docker exec -it $IMG chown -R www-data:www-data $TEMPLATE

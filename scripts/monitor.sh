# Monitors repository changes

# Goes to root directory
cd $(dirname $0)/..

while inotifywait -e modify -qr .; do
  sh scripts/build.sh 2>&1 1>/dev/null
  sh scripts/add.sh 2>&1 1>/dev/null
done
